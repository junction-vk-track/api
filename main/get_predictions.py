import numpy as np
import pandas as pd
import surprise
import vk_api

def get_predictions(user_ids, items_ids, values, target_user_id, user_items, n_recommend):
    """
    Return two arrays of item indices of length n_recommend:
    1. Only active wishes
    2. Considering already bought products
    These 2 array can intersect

    Parameters
    ----------
    user_ids : list(string)
    item_ids : list(string)
    values: (1 for active wishes, 2 for past wishes)

    user_id : string
        Id of target user
    user_items: list
        list of items user have
    n_recommend : int
        len of recommendation array

    Raises
    ------
    Exception
        Size of available items less than len of best predictions

    Returns
    -------
    maxids_new, maxids_all
        Two arrays of item indices
    """

    if n_recommend > len(set(items_ids)):
        raise Exception('Not enough products to build {:} predictions'.format(n_recommend))

    ratings_dict = {'userID': user_ids,
                    'itemID': items_ids,
                    'rating': values}
    df = pd.DataFrame(ratings_dict)

    df.at[df['rating'] != 0, 'rating'] = 1

    reader = surprise.Reader()
    data = surprise.Dataset.load_from_df(df[['userID', 'itemID', 'rating']], reader)
    alg = surprise.SVD()
    alg.fit(data.build_full_trainset())

    maxids_new = ['0']*n_recommend
    maxs = np.zeros(n_recommend)
    for i in set(items_ids):
        if i not in user_items:
            rate = alg.predict(target_user_id, i)[3]
            for jnd, j in enumerate(maxs):
                if rate > j:
                    tmp_m = np.insert(maxs, jnd, rate)
                    maxids_new.insert(jnd, i)
                    break
            maxs = tmp_m[:n_recommend]
            maxids_new = maxids_new[:n_recommend]

    df = pd.DataFrame(ratings_dict)
    # df = df[~(df['rating'] == 2)]
    df = df[(df['rating'] == 1)]

    reader = surprise.Reader()
    data = surprise.Dataset.load_from_df(df[['userID', 'itemID', 'rating']], reader)
    alg = surprise.SVD()
    alg.fit(data.build_full_trainset())

    maxids_all = ['0']*n_recommend
    maxs = np.zeros(n_recommend)
    for i in set(items_ids):
        if i not in user_items:
            rate = alg.predict(target_user_id, i)[3]
            for jnd, j in enumerate(maxs):
                if rate > j:
                    tmp_m = np.insert(maxs, jnd, rate)
                    maxids_all.insert(jnd, i)
                    break
            maxs = tmp_m[:n_recommend]
            maxids_all = maxids_all[:n_recommend]
    return maxids_new, maxids_all


def get_vk_recommendation(token):
    vk_session = vk_api.VkApi(token=token)
    vk = vk_session.get_api()
    return vk.junction.getFeed(count = 10)
