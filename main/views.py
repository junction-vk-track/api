import json

from django.http import Http404, HttpRequest, HttpResponseBadRequest, JsonResponse
from django.shortcuts import render

# Create your views here.
from main.get_predictions import get_predictions
from main.models import VkUser, AliexpressProduct, Wish


def add_to_wishlist(request):
    if request.method != "POST":
        return HttpResponseBadRequest("Non-post method")
    json_data = json.loads(request.body)
    try:
        user_id = json_data["user_id"]
        product_json = json_data["product"]
        product_id = int(product_json["product_id"])

        obj, created = VkUser.objects.get_or_create(user_id=user_id)

        pid = product_json.get("id", 0)
        url = product_json.get("url", "")
        name = product_json.get("name", "")
        price = product_json.get("price", 0.0)
        old_price = product_json.get("old_price", 0.0)
        currency_id = product_json.get("currency_id", 0)
        category_id = product_json.get("category_id", 0)
        description = product_json.get("description", "")
        product, created = AliexpressProduct.objects.update_or_create(
            id=pid, url=url, product_id=product_id, name=name, price=price,
            old_price=old_price, currency_id=currency_id, description=description,
            category_id=category_id
        )

        wish = Wish.objects.update_or_create(aliexpress_product=product, wisher=obj, bought=False)
        return JsonResponse({"success": True})
    except Exception as e:
        return JsonResponse({"success": False, "exception": str(e)})


def remove_from_wishlist(request):
    if request.method != "POST":
        return HttpResponseBadRequest("Non-post method")
    json_data = json.loads(request.body)
    try:
        user_id = json_data["user_id"]
        product_id = int(json_data["product_id"])

        Wish.objects.filter(wisher__user_id=user_id, aliexpress_product__product_id=product_id).delete()
        return JsonResponse({"success": True})
    except Exception as e:
        return JsonResponse({"success": False, "exception": str(e)})


def buy(request):
    if request.method != "POST":
        return HttpResponseBadRequest("Non-post method")
    json_data = json.loads(request.body)
    try:
        user_id = json_data["user_id"]
        product_id = int(json_data["product_id"])

        item = Wish.objects.get(wisher__user_id=user_id, aliexpress_product__product_id=product_id)
        item.bought = True
        item.save()
        return JsonResponse({"success": True})
    except Exception as e:
        return JsonResponse({"success": False, "exception": str(e)})


def get_wishlist(request, id):
    if request.method != "GET":
        return HttpResponseBadRequest("Non-get method")
    try:
        user_id = id
        wishes = Wish.objects.filter(wisher__user_id=user_id).values()
        return JsonResponse({"wishlist": list(wishes)})
    except Exception as e:
        return JsonResponse({"success": False, "exception": str(e)})


def get_recommendations(request, id):
    if request.method != "GET":
        return HttpResponseBadRequest("Non-get method")
    try:
        users = VkUser.objects.all()
        user_ids = []
        item_ids = []
        values = []
        target_user_id = id
        user_items = Wish.objects.filter(wisher__user_id=target_user_id).values()

        for user in users:
            wishes = Wish.objects.filter(wisher__user_id=user.user_id)
            for wish in wishes:
                status = 2 if wish.bought else 1
                user_ids.append(str(user.user_id))
                item_ids.append(str(wish.aliexpress_product_id))
                values.append(status)
        actual, every = get_predictions(user_ids, item_ids, values, target_user_id, user_items,
                                                            n_recommend=5)
        return JsonResponse({"actual": actual, "all": every})
    except Exception as e:
        return JsonResponse({"success": False, "exception": str(e)})