from django.db import models

# Create your models here.
from django.utils.timezone import now


class VkUser(models.Model):
    user_id = models.TextField(verbose_name="Vk user id", primary_key=True)


class AliexpressProduct(models.Model):
    id = models.BigIntegerField()
    product_id = models.BigIntegerField(primary_key=True)
    url = models.URLField()
    name = models.TextField()
    price = models.FloatField()
    old_price = models.FloatField()
    currency_id = models.BigIntegerField()
    description = models.TextField()
    category_id = models.BigIntegerField()


class Wish(models.Model):
    aliexpress_product = models.ForeignKey(AliexpressProduct, models.DO_NOTHING)
    bought = models.BooleanField(default=False)

    wisher = models.ForeignKey(VkUser, on_delete=models.CASCADE)


