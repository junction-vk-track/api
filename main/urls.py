from django.conf.urls import url
from django.contrib import admin
from django.urls import path, include

from main import views

urlpatterns = [
    path('add_to_wishlist/', views.add_to_wishlist),
    path('remove_from_wishlist/', views.remove_from_wishlist),
    path('get_wishlist/<str:id>/', views.get_wishlist),
    path('get_recommendations/<str:id>/', views.get_recommendations),
    path('buy/', views.buy),
]
